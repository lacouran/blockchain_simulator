import styled from 'styled-components';

export const SimulatorPageContainer = styled.div`
  background: linear-gradient(180deg, #0f0f0f 66%, black 100%);
`;