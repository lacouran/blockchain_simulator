import React, { useState } from 'react';
import Navbar from '../../components/Navbar';
import { SimulatorPageContainer } from './SimulatorElements';
import { ThemeProvider } from 'styled-components';
import { ExpansionColors } from '../../components/colors';
import Simulator from '../../components/Simulator';

import { transitions, positions, Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic'


const SimulatorPage = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  };

  const options = {
    // you can also just use 'bottom center'
    position: positions.BOTTOM_CENTER,
    timeout: 5000,
    offset: '1rem',
    transition: transitions.FADE
  };

  return (
    <ThemeProvider theme={ExpansionColors}>
      <AlertProvider template={AlertTemplate} {...options}>
        <SimulatorPageContainer>
          <Navbar toggle={toggle} />
          <Simulator />
        </SimulatorPageContainer>
      </AlertProvider>
    </ThemeProvider>
  );
};

export default SimulatorPage;
