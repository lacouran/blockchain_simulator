import React, { useState } from 'react';
import Navbar from '../../components/Navbar';
import Hero from '../../components/Hero';
import { IndexContainer } from './HomeElements';
import { ThemeProvider } from 'styled-components';
import { ExpansionColors } from '../../components/colors';
import Intro from '../../components/Intro';
import Tech from '../../components/Tech';
import Simulator from '../../components/Simulator';
import Accordion from '../../components/Accordion';
import Application from '../../components/Application';
import Footer from '../../components/Footer';
import Sidebar from '../../components/Sidebar';
import { transitions, positions, Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic'

const Home = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  }

  const options = {
    // you can also just use 'bottom center'
    position: positions.BOTTOM_CENTER,
    timeout: 5000,
    offset: '1rem',
    transition: transitions.FADE
  };

  return (
    <ThemeProvider theme={ExpansionColors}>
      <AlertProvider template={AlertTemplate} {...options}>
        <IndexContainer>
          <Sidebar isOpen={isOpen} toggle={toggle} />
          <Navbar toggle={toggle} />
          <Hero />
          <Intro />
          <Tech />
          <Application />
          <Accordion />
          <Simulator />
          <Footer />
        </IndexContainer>
      </AlertProvider>
    </ThemeProvider>
  )
}

export default Home
