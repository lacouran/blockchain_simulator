import React, { useState, useEffect } from "react";
import {
  SimulatorContainer,
  SimulatorContent,
  Block,
  BlockInfo,
  BlockchainButton,
  Transactions,
  BlockTransactions,
  PendingTransactions,
  AccountList,
  SubmitCustom,
  SelectCustom,
  AccordionHeader,
  AccordionContent,
  SectionTitle,
  CurrentTransaction,
  TransactionSection,
  TransactionList,
  BlockchainSection,
} from "./SimulatorElements";
import { SectionH1, SectionH2 } from "../SharedElements";
import { useAlert } from "react-alert";

import { FiMinus, FiPlus } from "react-icons/fi";
import { IconContext } from "react-icons/lib";

const Simulator = () => {
  const [blockchain, setBlockchain] = useState([]);

  const [block, setBlock] = useState([]);

  const [currentTransactions, setCurrentTransactions] = useState([]);

  const [lastTransaction, setLastTransaction] = useState([]);

  const [accounts, setAccounts] = useState([]);
  const [accountsWithAmount, setAccountsWithAmount] = useState([]);
  const [minerAccounts, setMinerAccounts] = useState([]);

  const [sender, setSender] = useState("");
  const [recipient, setRecipient] = useState("");
  const [amount, setAmount] = useState(0);
  const [fees, setFees] = useState(0);

  const [walletCrediter, setWalletCrediter] = useState("");
  const [amountToCredit, setAmountToCredit] = useState("");

  const [clicked, setClick] = useState(false);
  const [clickedMiner, setClickMiner] = useState(false);
  const alert = useAlert();

  const toogle = (miner) => {
    if (!miner) {
      if (clicked === true) {
        return setClick(false);
      }
      setClick(true);
    }
    else {
      if (clickedMiner === true) {
        return setClickMiner(false);
      }
      setClickMiner(true);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = { sender, recipient, amount, fees };
    newTransaction(data);
  };

  const handleCreditAccount = (e) => {
    e.preventDefault();
    const data = { walletCrediter, amountToCredit };
    creditAccount(data);
  };

  useEffect(() => {
    const headers = { "Content-Type": "application/json" };
    fetch("http://localhost:5000/pow/chain", { headers })
      .then((response) => response.json())
      .then((data) => setBlockchain(data.chain));
    // console.log(blockchain);
  }, [block]);

  useEffect(() => {
    const headers = { "Content-Type": "application/json" };
    fetch("http://localhost:5000/pow/transactionActuel", { headers })
      .then((response) => response.json())
      .then((data) => setCurrentTransactions(data));
    // console.log(currentTransactions);
  }, [lastTransaction, block, blockchain]);

  useEffect(() => {
    const headers = { "Content-Type": "application/json" };
    fetch("http://localhost:5000/pow/listaddress", { headers })
      .then((response) => response.json())
      .then((data) => setAccounts(data));
    // console.log(accounts);
    fetch("http://localhost:5000/pow/dictAddress", { headers })
      .then((response) => response.json())
      .then((data) => setAccountsWithAmount(data));
    fetch("http://localhost:5000/pow/listMineur", { headers })
      .then((response) => response.json())
      .then((data) => setMinerAccounts(data));
  }, [block, blockchain]);

  const mine = () => {
    const headers = { "Content-Type": "application/json" };
    fetch("http://localhost:5000/pow/mine", { headers })
      // .then((response) => response.json())
      // .then((data) => setBlock(data));
      .then(async (response) => {
        const isJson = response.headers
          .get("content-type")
          ?.includes("application/json");
        const data = isJson && (await response.json());

        // check for error response
        if (!response.ok) {
          // get error message from body or default to response status
          const error = (data && data.message) || response.status;
          alert.error(`Erreur : ${error}`);
          return Promise.reject(error);
        } else {
          setBlock(data);
          alert.success("Un nouveau bloc a été miné.")
        }
      })
      .catch((error) => {
        console.error("There was an error!", error);
        alert.error("Il n'y a pas assez de transactions pour miner un bloc");
      });
    // console.log(block);
  };

  const newTransaction = (data) => {
    const requestOptions = {
      method: "POST",
      // headers: { "Content-Type": "application/json" },
      // body: JSON.stringify(),
    };

    if (accounts.includes(data.sender) && accounts.includes(data.recipient)) {
      fetch(
        `http://localhost:5000/pow/transaction/new?sender=${data.sender}&recipient=${data.recipient}&amount=${data.amount}&fees=${data.fees}`,
        requestOptions
      )
        .then((response) => response.json())
        .then((data) => {
          setLastTransaction(data);
          alert.success("La nouvelle transaction a été ajoutée.");
        });
    } else {
      console.error("Wrong transaction");
      alert.error("Une des adresses n'existe pas sur la blockchain");
    }
  };

  const creditAccount = (dataToCredit) => {
    const requestOptions = {
      method: "POST",
      // headers: { "Content-Type": "application/json" },
      // body: JSON.stringify(),
    };
    console.log(dataToCredit.walletCrediter);

    if (accounts.includes(dataToCredit.walletCrediter)) {
      fetch(
        `http://localhost:5000/pow/credidateAddress?amount=${dataToCredit.amountToCredit}&address=${dataToCredit.walletCrediter}`,
        requestOptions
      )
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          alert.success(`Vous avez creditez ${dataToCredit.amountToCredit} token(s) sur ${dataToCredit.walletCrediter}`);
        });
    } else {
      console.error("Wrong transaction");
      alert.error("Une des adresses n'existe pas sur la blockchain");
    }
  };

  const reset = () => {
    const headers = { "Content-Type": "application/json" };
    fetch("http://localhost:5000/pow/reset", { headers })
      .then((response) => response.json())
      .then((data) => {
        setBlockchain(data.chain);
        alert.success("Blockchain reset avec succès.");
      });
    console.log(blockchain);
  };

  const shortenAddress = (address, chars = 4) => {
    return `${address.slice(0, chars)}...${address.slice(-chars)}`;
  };

  return (
    <SimulatorContainer id="simulator">
      <SectionH1>Simulateur</SectionH1>
      <SimulatorContent>
        <BlockchainButton onClick={reset}>
          Reset the Blockchain
        </BlockchainButton>
        <SectionTitle>Blockchain</SectionTitle>
        <BlockchainSection>
          {blockchain.map((block, index) => {
            return (
              <Block key={index}>
                <BlockInfo>
                  <li id="block-tittle">Block index : {block.index}</li>
                  {index != 0 ? (
                    <li>
                      {/* Blockhash : {shortenAddress(blockchain[index].previous_hash.toString())} */}
                      Blockhash :{" "}
                      {shortenAddress(block.previous_hash.toString())}
                    </li>
                  ) : (
                    <li>Genesis Block</li>
                  )}
                  {index > 1 ? (
                    <li>
                      {/* Previous blockhash : {shortenAddress(blockchain[index].previous_hash.toString())} */}
                      Previous blockhash :{" "}
                      {shortenAddress(block.previous_hash.toString())}
                    </li>
                  ) : (
                    <li>Previous blockhash : Genesis Block</li>
                  )}
                  {/* <li>Previous blockhash : {index != 0 && shortenAddress(blockchain[index].previous_hash)}</li> */}
                  <li>Proof : {block.proof}</li>
                  {/* <li>Timestamp : {shortenAddress(blockchain[index].timestamp.toString())}</li> */}
                  <li>
                    Timestamp : {shortenAddress(block.timestamp.toString())}
                  </li>
                </BlockInfo>
                <BlockTransactions>
                  {block.transactions.map((transaction, transactionIndex) => {
                    return (
                      <Transactions key={transactionIndex}>
                        <li>Sender : {shortenAddress(transaction.sender)}</li>
                        {/* <li>Recipient : {shortenAddress(block.transactions[transactionIndex].recipient)}</li> */}
                        <li>
                          Recipient : {shortenAddress(transaction.recipient)}
                        </li>
                        <li>Amount : {transaction.data}</li>
                        <li>Fees : {transaction.fees}</li>
                      </Transactions>
                    );
                  })}
                </BlockTransactions>
              </Block>
            );
          })}
        </BlockchainSection>
        <BlockchainButton onClick={mine}>Mine a new Block</BlockchainButton>
        <SectionTitle>Transactions</SectionTitle>
        <TransactionSection>
          <CurrentTransaction>
            <SectionTitle>Create new transaction</SectionTitle>
            <form onSubmit={handleSubmit}>
              {/* <SubmitCustom
                type="text"
                required
                value={sender}
                placeholder="Sender"
                onChange={(e) => setSender(e.target.value)}
              /> */}
              <SelectCustom
                type="text"
                required
                value={sender}
                placeholder="Sender"
                onChange={(e) => setSender(e.target.value)}
              >
                {accounts.map((account, key) => (
                  <option value={account}>{account}</option>
                ))}
              </SelectCustom>
              {/* <SubmitCustom
                type="text"
                required
                value={recipient}
                placeholder="Recipient"
                onChange={(e) => setRecipient(e.target.value)}
              /> */}
              <SelectCustom
                type="text"
                required
                value={recipient}
                placeholder="Recipient"
                onChange={(e) => setRecipient(e.target.value)}
              >
                {accounts.map((account, key) => (
                  <option value={account}>{account}</option>
                ))}
              </SelectCustom>
              <SubmitCustom
                type="number"
                min="1"
                required
                value={amount}
                placeholder="Amount"
                onChange={(e) => setAmount(e.target.value)}
              />
              <SubmitCustom
                type="number"
                min="1"
                required
                value={fees}
                placeholder="Tips"
                onChange={(e) => setFees(e.target.value)}
              />
              <SubmitCustom type="submit" value="Create transaction" />
            </form>
          </CurrentTransaction>
          <PendingTransactions>
            <SectionTitle>Pending transactions</SectionTitle>
            <TransactionList>
              {currentTransactions.map((transaction, transactionIndex) => {
                return (
                  <Transactions key={transactionIndex}>
                    <li>Sender : {shortenAddress(transaction.sender)}</li>
                    {/* <li>Recipient : {shortenAddress(block.transactions[transactionIndex].recipient)}</li> */}
                    <li>Recipient : {shortenAddress(transaction.recipient)}</li>
                    <li>Amount : {transaction.data}</li>
                    <li>Fees : {transaction.fees}</li>
                  </Transactions>
                );
              })}
            </TransactionList>
          </PendingTransactions>
        </TransactionSection>
        <SectionTitle>Accounts</SectionTitle>
        <AccountList>
          <IconContext.Provider value={{ color: "#2EFAFA", size: "50px" }}>
            <AccordionHeader onClick={() => toogle(false)}>
              <h1>Lister les adresses</h1>
              <span>{clicked === true ? <FiMinus /> : <FiPlus />}</span>
            </AccordionHeader>
            <AccordionContent show={clicked === true}>
              {accountsWithAmount.map((account, accountIndex) => {
                return <li key={accountIndex}>{account.address} : {account.balance}</li>;
              })}
            </AccordionContent>
          </IconContext.Provider>
          <IconContext.Provider value={{ color: "#2EFAFA", size: "50px" }}>
            <AccordionHeader onClick={() => toogle(true)}>
              <h1>Lister les mineurs</h1>
              <span>{clickedMiner === true ? <FiMinus /> : <FiPlus />}</span>
            </AccordionHeader>
            <AccordionContent show={clickedMiner === true}>
              {minerAccounts.map((account, accountIndex) => {
                return <li key={accountIndex}>{account.address} : {account.balance}</li>;
              })}
            </AccordionContent>
          </IconContext.Provider>
        </AccountList>
        <SectionTitle>Credit accounts</SectionTitle>
        <form onSubmit={handleCreditAccount}>
          <SelectCustom
            type="text"
            required
            value={walletCrediter}
            placeholder="Acount"
            onChange={(e) => setWalletCrediter(e.target.value)}
          >
            {accounts.map((account, key) => (
              <option value={account}>{account}</option>
            ))}
          </SelectCustom>
          <SubmitCustom
            type="number"
            min="1"
            required
            value={amountToCredit}
            placeholder="Amount"
            onChange={(e) => setAmountToCredit(e.target.value)}
          />
          <SubmitCustom type="submit" value="Credit tokens" />
        </form>
      </SimulatorContent>
    </SimulatorContainer>
  );
};

export default Simulator;
