import styled from 'styled-components';

export const SimulatorContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  min-height: 100vh;
  padding-bottom: 50px;
  width: 100%;
  color: white;
`;

export const SimulatorContent = styled.div`
  display: flex;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 80%;
  color: white;
`;

export const SectionTitle = styled.h2`
    font-family: 'Chakra Petch', sans-serif;
    font-size: 2rem;
    margin-top: 50px;
    text-align: center;
`;


export const Content = styled.div`
  p {
    font-size: 1.1rem;
    text-align: justify;

    &#example-tittle {
      padding-top: 10px;
      font-weight: 700;
    }
  }
`;

export const BlockchainButton = styled.button`
  max-width: 300px;
  width: 100%;
  height: 80px;
  background: #0f0f0f;
  border: 2px solid ${props => props.theme.secondary};
  border-radius: 5px;
  color: white;
  font-family: 'Chakra Petch', sans-serif;
  font-size: 1.75rem;
  font-weight: bold;
  text-transform: uppercase;
  transition: transform 0.5s ease-in-out;
  /* text-shadow: 0px 0px 15px ${props => props.theme.secondary}; */
  top: calc(50% - 40px);
  bottom: 50%;
  /* cursor: pointer; */
  position: relative;
  z-index: 0;

  ::after {
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    background: ${props => props.theme.secondary};
    border-radius: 5px;
    /* box-shadow: 0px 0px 50px 10px ${props => props.theme.secondary}; */
    opacity: 0;
    transition: opacity 0.5s ease-in-out;
    z-index: -1;
  }
  @media (hover) {
    &:hover {
      /* transform: scale(1.03); */
      transition: opacity 0.5s ease-in-out;
      cursor: pointer;
      ::after {
          opacity: 1;
      }
    }
  }

`;

export const BlockchainSection = styled.div`
  margin: 10px 0 80px 0;
`;

export const BlockChain = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;

  h2 {
    font-family: 'Chakra Petch', sans-serif;
    font-size: 2rem;
    margin-top: 50px;
  }
`;

export const Block = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  @media screen and (max-width: 768px) {
    flex-direction: column;
  }
`;

export const BlockInfo = styled.ul`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  border: solid 2px #fff;
  /* height: 100px; */
  width: 250px;
  margin: 40px;
  padding: 10px;
  list-style-type: none;

  position: relative;
  z-index: 1;

  :before {
    content: '';
    position: absolute;
    height: 80px;
    bottom: calc(0% - 82px);
    right: 50%;
    color: white;
    width: 5px;
    background: white;
    z-index: 2;
  }
/* 
  :after {
    content: '';
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    background: linear-gradient(180deg, transparent 40%, rgba(0,0,0,1) 100%),
    linear-gradient(180deg, rgba(0,0,0,1) 0%, transparent 60%);
    z-index: 2;
  } */

  li {
    /* margin: 0; */
    &#block-tittle {
      padding-bottom: 10px;
      font-size: 1.2rem;
      font-weight: 700;
    }
  }

  @media screen and (max-width: 768px) {
    :before {
      display: none;
  }
  }
`;

export const BlockTransactions = styled.ul`
  display: flex;
  /* flex-direction: column; */
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: flex-start;
  border-left: solid 2px #fff;
  /* height: 100px; */
  width: 300px;
  /* margin: 40px; */
  padding: 10px;
  list-style-type: none;

  li {
    /* margin: 0; */
  }
`;

export const TransactionSection = styled.div`
  display: flex;
  justify-content: center:
  align-items: flex-start;
  width: 100%;
`;

export const PendingTransactions = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start:
  align-items: center;
  width: 50%;
  padding: 0 10px;
`;

export const TransactionList = styled.div`
  display: flex;
  justify-content: flex-start:
  align-items: flex-start;
  flex-wrap: wrap;
`;

export const CurrentTransaction = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start:
  align-items: center;
  width: 50%;
  padding: 0 10px;

  form {
    display: flex;
    flex-direction: column;
    justify-content: center:
    align-items: center;
    width: 90%;
  }
`;

export const Transactions = styled.ul`
  padding: 8px;
  list-style-type: none;
  margin: 0;
  
  li {
    font-size: 0.7rem;
  }
`;

export const AccountList = styled.div`
  width: 100%;
  max-width: 700px;
`;

export const SubmitCustom = styled.input`
  background: #0f0f0f;
  padding: 20px;
  margin: 5px;
  border: 2px solid ${props => props.theme.secondary};
  border-radius: 5px;
  color: white;
  font-family: 'Chakra Petch', sans-serif;
  font-size: 1rem;
  font-weight: bold;
  cursor: pointer;
`;

export const SelectCustom = styled.select`
  background: #0f0f0f;
  padding: 20px;
  margin: 5px;
  border: 2px solid ${props => props.theme.secondary};
  border-radius: 5px;
  color: white;
  font-family: 'Chakra Petch', sans-serif;
  font-size: 1rem;
  font-weight: bold;
  cursor: pointer;
`;

export const AccordionHeader = styled.div`
  background: transparent;
  color: white;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  text-align: left;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  border-bottom: 2px solid ${props => props.theme.secondary};

  h1 {
    padding: 2rem;
    font-size: 1.5rem;
    font-family: 'Bai Jamjuree', sans-serif;

    @media screen and (max-width: 480px ) {
      font-size: 1.2rem;
    }
  }

  span {
    margin-right: 1.5rem;
  }

  &:hover {
    transition: all 0.2s ease-in-out;
    transform: scale(1.05);

    @media screen and (max-width: 480px) {
      transform: none;
    }
  }
`;

export const AccordionContent = styled.ul`
  background: transparent;
  color: white;
  width: 100%;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  text-align: center;
  list-style-type: none;
  padding: 0;

  overflow: hidden;
  /* transition: all 600ms ease-in-out; */

  max-height: ${({show}) => (show ? '1000px' : '0')};
  opacity: ${({show}) => (show ? '1' : '0')};

  grid-gap: 5px;

  li {
    font-size: 0.7rem;
    /* width: 85%; */
    padding: 0;

    @media screen and (max-width: 880px) {
      font-size: 1rem;
    }
  }
`;