import styled, { keyframes, css } from "styled-components";
import { Link as LinkR } from 'react-router-dom';
import { Link as LinkS } from 'react-scroll';

export const Nav = styled.nav`
  background: ${({ scrollNav }) => (scrollNav ? '#0f0f0f' : 'transparent')};
  height: 80px;
  /* margin-top: -80px; */
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-size: 1rem;
  position: fixed;
  top: 0;
  z-index: 11;
  /* transition: 0.8s all ease; */
  width: 100%;

  @media screen and (max-width: 1000px) {
    /* transition: 0.8s all ease; */
  }
`;

export const NavProgressContainer = styled.div`
  width: inherit;
`;

export const NavProgress = styled.div`
  height: 5px;
  background: ${({ bottom }) => ( bottom ? 'rgb(46, 250, 250, 1)' : 'linear-gradient(to right, rgb(46, 250, 250, 1) 0%, rgb(46, 250, 250, 0.75) 75%, transparent 100%)')};
  width: ${({ scrollProgress }) => css`${scrollProgress}%`};
  transition: background 1s ease;
`;
  
export const NavbarContainer = styled.div`
  display: flex;
  justify-content: center;
  height: 75px;
  z-index: 1;
  width: 100%;
  max-width: 75%;

  @media screen and (max-width: 1000px) {
    justify-content: center;
  }
`;

const rotateY = keyframes`
  from {
    transform: rotateY(0deg);
  }

  to {
    transform: rotateY(360deg);
  }
`;

export const NavLogo = styled(LinkR)`
  justify-self: flex-start;
  cursor: pointer;
  display: flex;
  align-items: center;
  animation: ${rotateY} 4s ease infinite;

  img {
    width: 100px;
  }
`;

export const MobileIcon = styled.div`
  display: none;

  @media screen and (max-width: 1000px) {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(-100%, 60%);
    font-size: 1.8rem;
    cursor: pointer;
    color: white;
  }
`;

export const NavMenu = styled.ul`
  display: flex;
  align-items: center;
  justify-content: center;
  list-style: none;
  text-align: center;
  padding: 0;

  @media screen and (max-width: 1000px) {
    display: none;
  }
`;

export const NavItem = styled.li`
  height: 80px;
`;

export const NavLinks = styled(LinkS)`
  color: white;
  font-family: 'Chakra Petch', sans-serif;
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0 1rem;
  height: 100%;
  cursor: pointer;
  font-weight: 900;
  text-transform: uppercase;
  transition: transform 0.2s ease-in-out;

  &.active {
    border-bottom: 3px solid ${props => props.theme.secondary};
    color: ${props => props.theme.secondary};
    transform: translateY(-10px);
  }

  &:hover {
    transition: transform 0.2s ease-in-out;
    color: ${props => props.theme.secondary};
    transform: translateY(-10px);
  }
`;

export const NavButton = styled.nav`
  display: flex;
  align-items: center;

  @media screen and (max-width: 1000px) {
    display: none;
  }
`;