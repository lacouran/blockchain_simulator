import React, { useState, useEffect } from 'react';
import { animateScroll as scroll } from 'react-scroll';
import { FaBars } from 'react-icons/fa';
// import { IconContext } from 'react-icons/lib';
import { Nav, NavbarContainer, NavLogo, MobileIcon, NavMenu, NavItem, NavLinks, NavButton, NavProgressContainer, NavProgress, NavMerch } from './NavbarElements';
import { ThemeProvider } from 'styled-components'
import { ExpansionColors } from '../colors';

const Navbar = (props) => {
  const [scrollNav, setScrollNav] = useState(false);

  const changeNav = () => {
    if (window.scrollY >= 250) {
      setScrollNav(true);
    }
    else {
      setScrollNav(false);
    }
  }

  const [scrollProgress, setscrollProgress] = useState(0);
  const [bottom, setbottom] = useState(false);

  const changeProgress = () => {
    var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    var scrolled = (winScroll / height) * 100;
    if (scrolled >= 99) {
      setbottom(true);
    }
    else {
      setbottom(false);
    }
    setscrollProgress(scrolled);
  }

  useEffect(() => {
    window.addEventListener('scroll', changeNav);
    window.addEventListener('scroll', changeProgress);
    return () => {
      window.removeEventListener('scroll', changeNav);
      window.removeEventListener('scroll', changeProgress);
    }
  }, [])

  const toggleHome = () => {
    scroll.scrollToTop();
  }

  return (
    <ThemeProvider theme={ExpansionColors}>
      <Nav scrollNav={scrollNav}>
        <NavbarContainer>
          {/* <NavLogo to="/" onClick={toggleHome}>
            <img src={Logo} alt="logo" />
          </NavLogo> */}
          <MobileIcon onClick={props.toggle}>
            <FaBars />
          </MobileIcon>
          <NavMenu>
            <NavItem><NavLinks to="introduction" smooth={true} duration={500} spy={true} exact='true' offset={-80}>Introduction</NavLinks></NavItem>
            <NavItem><NavLinks to="tech" smooth={true} duration={500} spy={true} exact='true' offset={-80}>Technologie</NavLinks></NavItem>
            <NavItem><NavLinks to="application" smooth={true} duration={500} spy={true} exact='true' offset={-80}>Application</NavLinks></NavItem>
            <NavItem><NavLinks to="faq" smooth={true} duration={500} spy={true} exact='true' offset={-80}>F.A.Q.</NavLinks></NavItem>
            <NavItem><NavLinks to="simulator" smooth={true} duration={500} spy={true} exact='true' offset={-80}>Simulateur</NavLinks></NavItem>
          </NavMenu>
          {/* <NavButton>
            <ButtonJoin href="https://discord.gg/drakosunchained" target="_blank" rel="noopener noreferrer" aria-label="Discord" small={true}>
              Join
            </ButtonJoin>
          </NavButton> */}
        </NavbarContainer>
        <NavProgressContainer>
          <NavProgress scrollProgress={scrollProgress} bottom={bottom}></NavProgress>
        </NavProgressContainer>
      </Nav>
    </ThemeProvider>
  )
}

export default Navbar
