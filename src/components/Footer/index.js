import React from 'react';
// import { animateScroll as scroll } from 'react-scroll';
import { FaDiscord, FaInstagram, FaTwitter } from 'react-icons/fa';
import { FooterContainer, FooterBackground, FooterLinkTittle, ImageBackground, FooterContent, SocialMedia, WebsiteRights, SocialIcons, SocialIconLink } from './FooterElements';
import { ButtonJoin, ButtonJoinWrapper } from '../SharedElements';
// import Logo from "../../images/logo/drako-head-text.png";
import Image from "../../images/wallpaper/blockchain-footer2.png";


const Footer = () => {

  // const toggleHome = () => {
  //   scroll.scrollToTop();
  // }

  return (
    <FooterContainer id="footer">
      <FooterBackground>
        <ImageBackground src={Image} />
      </FooterBackground>
      <FooterContent>
        <SocialMedia>
          <ButtonJoinWrapper>
            <ButtonJoin href="https://www.hds.utc.fr/~wschon/dokuwiki/fr/biographie" target="_blank" rel="noopener noreferrer" aria-label="Walter Schon">
              Heudiasyc - Walter Schon
            </ButtonJoin>
          </ButtonJoinWrapper>
          <WebsiteRights>TX Blockchain - UTC - {new Date().getFullYear()}</WebsiteRights>
        </SocialMedia>
      </FooterContent>
    </FooterContainer>
  )
}

export default Footer
