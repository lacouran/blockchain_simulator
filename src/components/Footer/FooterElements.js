import styled, { keyframes } from "styled-components";
// import { Link } from 'react-router-dom';

export const FooterContainer = styled.footer`
  display: flex;
  flex-direction: column;
  position: relative;
  z-index: 1;

  :before {
    content: '';
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    background: linear-gradient(180deg, transparent 50%, black 100%), linear-gradient(0deg, transparent 50%, black 100%);
    z-index: 2;
  }
`;

export const FooterBackground = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
`;

const rotateY = keyframes`
  0%, 100% {
    opacity: 1;
  }

  50% {
    opacity: 0.3;
  }
`;

export const ImageBackground = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  object-position: top;
  animation: ${rotateY} 10s ease infinite;
`;

export const FooterContent = styled.div`
  z-index: 3;
  padding: 30px 24px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  max-width: 75%;
  width: 100%;
  margin: 0 auto;

  @media screen and (max-width: 768px) {
    max-width: 90%;
  }
`;

export const SocialMedia = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  /* grid-gap: 50px; */
`;

export const WebsiteRights = styled.small`
  color: white;
  text-align: center;
  margin: 64px 0 30px 0;
`;

export const SocialIcons = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  /* flex-wrap: wrap; */
  width: 250px;
  
  @media screen and (max-width: 820px) {
    margin-bottom: 16px;
  }
`;

export const SocialIconLink = styled.a`
  color: ${props => props.theme.secondary};
  transition : 0.2s ease-in-out;
  font-size: 3rem;
  /* width: 70px; */

  &:hover {
    color: ${props => props.theme.primary};
    transform: scale(1.2);
    transition : 0.2s ease-in-out;
  }

  @media screen and (max-width: 480px) {
    font-size: 2.5rem;
  }
`;

export const FooterLinkTittle = styled.h1`
  font-size: 2.5rem;
  text-align: center;
  margin-bottom: 50px;
  font-family: 'Akira Expanded', sans-serif;
  color: white;

  @media screen and (max-width: 480px) {
    font-size: 2rem;
  }
`;