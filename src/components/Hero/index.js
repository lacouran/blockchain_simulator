import React from "react";
import {
  HeroContainer,
  HeroBackground,
  VideoBackground,
  HeroContent,
  ImageBackground,
  Heroarrow,
} from "./HeroElements";
import Video from "../../videos/blockchain-hero-short.mp4";

import { IoIosArrowDown } from "react-icons/io";
import { IconContext } from "react-icons/lib";

const Hero = () => {
  return (
    <HeroContainer id="home">
      <HeroBackground>
        <VideoBackground
          autoPlay
          loop
          muted
          playsinline
          src={Video}
          vtype="video/mp4"
        />
      </HeroBackground>
      <HeroContent>
        <IconContext.Provider value={{ color: "#2EFAFA" }}>
          <Heroarrow
            to="introduction"
            smooth={true}
            duration={500}
            spy={true}
            exact="true"
            offset={-80}
          >
            <IoIosArrowDown />
          </Heroarrow>
        </IconContext.Provider>
      </HeroContent>
    </HeroContainer>
  );
};

export default Hero;
