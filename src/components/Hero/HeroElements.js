import styled, { keyframes } from "styled-components";
import { Link as LinkS } from 'react-scroll';

export const HeroContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: flex-end;
  padding: 0 30px;
  min-height: 100vh;
  position: relative;
  z-index: 1;

  :before {
    content: '';
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    background: linear-gradient(180deg, transparent 75%, black 100%);
    z-index: 2;
  }
`;

export const HeroBackground = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
`;

export const ImageBackground = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const HeroContent = styled.div`
  z-index: 3;
  max-width: 1200px;
  width: 100%;
  /* position: absolute; */
  padding: 0px 24px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;
  color: white;
`;

export const VideoBackground = styled.video`
  width: 100%;
  height: 100%;
  object-fit: cover;
  background: black;
  /* @media screen and (max-width: 768px) {
    display: none;
  } */
`;

export const HeroTittle = styled.h1`
  color: white;
  font-size: 48px;
  font-family: 'Aclonica', sans-serif;
  text-align: center;
  margin-bottom: 5rem;

  @media screen and (max-width: 768px) {
    font-size: 3rem;
  }

  @media screen and (max-width: 480px) {
    font-size: 2rem;
  }
`;

export const HeroText = styled.p`
  margin-top: -24px;
  margin-bottom: 5rem;
  color: white;
  font-size: 1.5rem;
  text-align: center;
  max-width: 600px;

  @media screen and (max-width: 480px) {
    font-size: 1rem;
  }
`;

export const HeroButtonWrapper = styled.div`
  margin-top: 32px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const bounce = keyframes`
  0%, 100% {
    transform: translateY(0);
  }

  50% {
    transform: translateY(-20px);
  }
`;

export const Heroarrow = styled(LinkS)`
  margin-bottom: 10vh;
  font-size: 50px;
  animation: ${bounce} 2s ease infinite;
  cursor: pointer;
`;