import React from 'react'
import { SidebarContainer, Icon, CloseIcon, SidebarWrapper, SidebarMenu, SidebarLink, SidebarButtonWrap, SidebarMerch } from './SidebarElements'
import { ButtonJoin } from '../SharedElements';

const Sidebar = (props) => {
  return (
    <SidebarContainer isOpen={props.isOpen}>
      <Icon onClick={props.toggle}>
        <CloseIcon />
      </Icon>
      <SidebarWrapper>
        <SidebarMenu>
          <SidebarLink to="introduction" onClick={props.toggle}>Introduction</SidebarLink>
          <SidebarLink to="tech" onClick={props.toggle}>Technologie</SidebarLink>
          <SidebarLink to="application" onClick={props.toggle}>Application</SidebarLink>
          <SidebarLink to="faq" onClick={props.toggle}>F.A.Q</SidebarLink>
          <SidebarLink to="simulator" onClick={props.toggle}>Simulateur</SidebarLink>
        </SidebarMenu>
        <SidebarButtonWrap>
          <ButtonJoin href="https://www.hds.utc.fr/~wschon/dokuwiki/fr/biographie" target="_blank" rel="noopener noreferrer" aria-label="Walter Schon">
            Heudiasyc - Walter Schon
          </ButtonJoin>
        </SidebarButtonWrap>
      </SidebarWrapper>
    </SidebarContainer>
  )
}

export default Sidebar
