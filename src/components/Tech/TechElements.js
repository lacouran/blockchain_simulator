import styled from 'styled-components';

export const TechContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  min-height: 100vh;
  width: 100%;
  padding-bottom: 50px;
`;

export const TechContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  width: 80%;
  color: white;
`;

export const Tittle = styled.h2`
  font-family: 'Chakra Petch', sans-serif;
  width: 100%;
  font-size: 2rem;
  margin: 2rem 0 1rem 0;
`;

export const SubTittle = styled.h3`
  font-family: 'Chakra Petch', sans-serif;
  width: 100%;
  font-size: 1.5rem;
  padding-top: 10px;
  font-weight: 700;
`;

export const Content = styled.div`
  p {
    font-size: 1.1rem;
    text-align: justify;

    &#example-tittle {
      padding-top: 10px;
      font-weight: 700;
    }

    &#legend {
      font-size: 0.8rem;
      text-align: center;
      /* padding-top: 0px; */
    }
  }

  img {
    display: flex;
    justify-content: center;
    width: 90%;
    margin: 50px auto 10px auto;

    &#hash {
      width: 50%;
    }

    &#supply {
      width: 50%;
      border: 3px solid ${props => props.theme.secondary};;
    }
  }
`;