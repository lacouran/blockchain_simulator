import React from "react";
import {
  TechContainer,
  TechContent,
  Tittle,
  Content,
  SubTittle,
} from "./TechElements";
import { SectionH1, SectionH2 } from "../SharedElements";

import BlockchainDiagram from "../../images/assets/blockchain-diagram.png";
import HashDiagram from "../../images/assets/hash-function.png";
import BitcoinSupply from "../../images/assets/bitcoin-supply.png";
import ForkInintentional from "../../images/assets/fork-inintentional.png";

const Tech = () => {
  return (
    <TechContainer id="tech">
      <SectionH1>Technologie</SectionH1>
      <TechContent>
        <Tittle>Contexte Historique</Tittle>
        <Content>
          <p>
            La Blockchain est comparable à une base de données distribuée de
            transaction. Elle a été implémentée pour la première fois en 2008
            par son créateur Satoshi Nakamoto, également inventeur du Bitcoin :
            première implémentation de cette technologie. Publié d’abord dans un
            White Paper en 2008 “Bitcoin: A Peer-to-Peer Electronic Cash
            System”, Nakamoto lance le réseau Bitcoin en 2009 avec ses premiers
            jetons. Contrairement à beaucoup d'idées reçues sur les fondements
            du Bitcoin, Nakamoto a affirmé prôné la liberté et la transparence
            avant toute chose. Le but principal de la blockchain est de
            supprimer les “intermédiaires de confiance”. En l'occurrence, dans
            le cadre d’un système de paiement, c’est la banque l'intermédiaire
            de confiance qui est supprimée. Nakamoto voyait cela comme un
            affranchissement. D'autres voient ça comme de l’anarchisme. Il n’est
            pas question dans cette TM de débattre sur l'intérêt et les
            vocations du Bitcoin, mais uniquement d’analyser la technologie
            derrière cette cryptomonnaie : la blockchain.
          </p>
        </Content>
        <Tittle>Fonctionnement général (exemple du Bitcoin)</Tittle>
        <Content>
          <p>
            Adresses, et transactions. Tels sont les 2 maîtres mots de la
            blockchain. En effet, une blockchain est très grossièrement, un
            registre de transactions entre des adresses. Ce registre est stocké
            dans des “blocs”, qui sont reliés un à un et forment donc une chaîne
            de blocs. Chaque bloc est constitué d’une série de données (cela
            change d’une blockchain à une autre). Cette chaîne de bloc est
            stockée dans des nœuds (“mineurs” ou “validateurs” selon la
            blockchain. On parle de mineurs pour le Bitcoin) qui constituent à
            eux le réseau peer-to-peer de la blockchain. Il s’agit
            d’ordinateurs, de serveurs répartis sur la planète.
          </p>
          <p>
            Dans le cadre de la blockchain Bitcoin, les données sont les
            suivantes :
          </p>
          <ul>
            <li>un index</li>
            <li>un hash servant à identifier le bloc</li>
            <li>le hash du bloc précédent</li>
            <li>un timestamp</li>
            <li>un ensemble de transactions</li>
          </ul>
          <img src={BlockchainDiagram} />
          <p id="legend">Contenu d'un bloc du Bitcoin</p>
          <p>
            Un bloc se ferme lorsque le nombre de transactions maximum par bloc
            est atteint. Pour se fermer, le bloc doit être “miné”, ou “validé”
            (dans le cadre du Bitcoin, on va parler de minage). Pour miner un
            bloc, il faut pouvoir résoudre un calcul complexe sur une fonction
            de hachage. La résolution de ce calcul se fait par brute-force : Le
            mineur doit tester sans prédiction, absolument toutes les
            combinaisons possibles pour obtenir le résultat attendu. Cela vient
            du caractère unilatéral de la fonction de hachage : d’une entrée, le
            résultat est trivial (pour un ordinateur). D’un résultat, l’entrée
            est introuvable.
          </p>
          <SubTittle>Minage d'un bloc</SubTittle>
          <p>
            Comme dit précédemment, un bloc contient, un index, un hash, le hash
            du bloc précédent, un timestamp ainsi qu’un ensemble de transaction.
            Le calcul à résoudre doit être le suivant : Partant de la
            concaténation de toutes les informations d’un bloc (formant le
            nombre y), la résolution de l’équation sera le nombre “z” qui ajouté
            à “y” et dont on applique la fonction de hachage SHA-256 donnera en
            sortie un nombre commençant par un certain nombre de zéro. Le nombre
            de 0 augmente tous les 210 000 (halving) blocs miné pour augmenter
            la difficulté de résolution et ralentir le rythme de minage du
            Bitcoin. Il est aujourd’hui à 19. Le premier mineur trouvant un
            nombre “z” valide, le soumet au reste des nœuds du réseau et valide
            le bloc : il ‘scelle le bloc’. Celui-ci obtient une récompense en
            guise de preuve de travail, qui se matérialise par une quantité
            définie de crypto actif. Cette récompense est divisée par 2
            (halving) tous les 210 000 blocs également.
          </p>
          <img src={HashDiagram} id="hash" />
          <p id="legend">
            Ici le nombre x represente la concaténation de y et z
          </p>
          <p id="example-tittle">Cicle de minage du Bitcoin</p>
          <p>
            Le white paper définit la quantité maximum de Bitcoin à 21 Millions.
            La quantité de BTC en circulation suit une courbe de charge d’un
            condensateur. Le premier BTC est entré en circulation le 3 janvier
            2009. La moitié de la supply a été miné en janvier 2013, et on
            estime que le dernier bitcoin sera miné en 2140.
          </p>
          <img src={BitcoinSupply} id="supply" />
          <p id="legend">
            Courbe du minage du bitoin ainsi que de son inflation
          </p>
        </Content>
        <Tittle>Système de validation des transaction</Tittle>
        <Content>
          <SubTittle>Proof of Work :</SubTittle>
          <p>
            Le système de validation vu précédemment est ce qu’on appelle le
            “proof of work” (preuve de travail. En effet, la validation du bloc
            se fait en recrutant un quantité de calcul pour résoudre l’équation.
            On parle de travail car le calcul est un travail coûteux : cela
            nécessite une puissance de calcul et donc indirectement des
            ressources financières.
          </p>
          <SubTittle>Proof of Stake :</SubTittle>
          <p>
            Au fur et à mesure que le Bitcoin a croît en popularité et que les
            mineurs se font de plus en plus nombreux (et que le Halving tous les
            210 000 blocs augmente la difficulté des équations), il n’a pas
            fallu longtemps pour qu’un nouveau système de validation voit le
            jour. En effet, les retombées écologiques du minage de bitcoin sont
            désastreuses (on estime que le minage du Bitcoin consomme presque
            autant qu’un pays du nord comme la France par an). C’est pourquoi
            les ingénieurs blockchains ont désigné un nouveau système de
            validation : le proof of stake. Dans ce système, les mineurs sont
            désormais appelés des validateurs, et le minage de bloc est donc une
            validation de bloc. La validation d’un bloc ne se fait plus par une
            résolution d’équation coûteuse mais par en mettant en jeu l’assets
            associé à la blockchain. On parle de vérouillage (“staking” en
            anglais) de crypto-actifs au profit de la sécurisation de la
            blockchain (validation de blocs et approbation de transactions).
            Plus un nœud validateur détient de crypto-actifs verrouillés, plus
            son poids sera important. Le choix du nœud qui pourra valider le
            bloc se fait via un algorithme choisissant via une pondération un
            nœud au hasard. Ce dernier recevra ainsi la récompense du proof of
            stake.
          </p>
        </Content>
        <Tittle>Evolutibilité de la blockchain</Tittle>
        <Content>
          <SubTittle>Le Fork accidentel</SubTittle>
          <p>
            Un fork se produit lorsque 2 mineurs valident le dernier bloc en
            même bloc. Ainsi, 2 blocs sont créés avec le même parent. La chaîne
            se divise alors, et il se crée un conflit. La blockchain devant être
            constituée de bloc avec uniquement 1 parent, une des 2 branches doit
            être annulée. Dès qu’une des 2 branches devient plus grande que
            l’autre, alors le réseau la sélectionne et invalide l’autre branche.
            Elle invalide donc également l’ensemble des transactions des blocs
            invalidés.
          </p>
          <img src={ForkInintentional} id="hash" />
          <p id="legend">
            Dans cet example, 2 mineurs trouvent la solution au bloc 767 en même
            temps
          </p>
          <SubTittle>Le Fork intentionnel</SubTittle>
          <p id="example-tittle">Soft Fork</p>
          <p>
            Un soft fork est une mise à jour de la blockchain. Généralement des
            ajouts de nouvelles fonctionnalités, les soft fork sont
            rétrocompatibles avec les anciens bloc du réseaux. En effet, un soft
            fork est restrictif : il s’agit soit de la restriction des règles de
            consensus, soit de l’ajout d’une nouvelle règle. Lorsque l’ensemble
            des contributeurs du réseau adoptent ces règles, le réseau continue
            son chemin sans se diviser.
          </p>
          <p id="example-tittle">Hard Fork</p>
          <p>
            Un hard fork est une mise à jour non rétrocompatible de la
            blockchain. Il est extensif : élargissement des règles du consensus.
            Ou bien bilatéral : incompatibilité des 2 côtés. Le hard fork
            nécessite que tous les nœuds du réseau se mettent à jour pour garder
            la blockchain intacte. Si un groupe d’acteur refuse d’adopter les
            nouvelles règles du consensus, alors la blockchain est divisée, et 2
            blockchains se crées à partir d’un même historique. C’est ce qui est
            arrivé de nombreuses fois avec le Bitcoin : le Bitcoin Gold par
            exemple est issu d’un hard fork du bitcoin qui a laissé la
            communauté partagé.
          </p>
        </Content>
      </TechContent>
    </TechContainer>
  );
};

export default Tech;
