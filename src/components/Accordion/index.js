import React, { useState } from 'react';
import { AccordionSection, Container, AccordionHeader, AccordionContent, AccordionItem, ExchangeLink } from './AccordionElements';
import { AccordionData } from './Data';
import { FiMinus, FiPlus } from 'react-icons/fi';
import { IconContext } from 'react-icons/lib';

import { SectionH1 } from '../SharedElements';

const Accordion = () => {

  const [clicked, setClick] = useState(false);

  const toogle = (index) => {
    if (clicked === index) {
      return setClick(null)
    }
    setClick(index);
  }
  return (
    <AccordionSection id="faq">
      <IconContext.Provider value={{ color: '#2EFAFA', size: '50px' }}>
        <Container>
          <SectionH1>F.A.Q.</SectionH1>
          <AccordionItem>
            {AccordionData.map((item, index) => {
              return (
                <div key={index}>
                  <AccordionHeader onClick={() => toogle(index)}>
                    <h1>{item.question}</h1>
                    <span>{(clicked === index ? <FiMinus /> : <FiPlus />)}</span>
                  </AccordionHeader>
                  <AccordionContent show={clicked === index}>
                    <p>{item.answer}</p>
                  </AccordionContent>
                </div>
              )
            })}
          </AccordionItem>
        </Container>
      </IconContext.Provider>
    </AccordionSection>
  )
}

export default Accordion
