import styled from "styled-components";

export const AccordionSection = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  position: center;
  padding-bottom: 50px;
`;

export const Container = styled.div`
  /* position: absolute; */
  /* width: 100%; */
  max-width: 75%;
  display: flex;
  flex-direction: column;
  align-items: center;

  @media screen and (max-width: 768px) {
    max-width: 90%;
  }
`;

export const AccordionItem = styled.div`
  display: flex;
  flex-direction: column;
`;

export const AccordionHeader = styled.div`
  background: transparent;
  color: white;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  text-align: left;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  border-bottom: 2px solid ${props => props.theme.secondary};

  h1 {
    padding: 2rem;
    font-size: 1.5rem;
    font-family: 'Bai Jamjuree', sans-serif;

    @media screen and (max-width: 480px ) {
      font-size: 1.2rem;
    }
  }

  span {
    margin-right: 1.5rem;
  }

  &:hover {
    transition: all 0.2s ease-in-out;
    transform: scale(1.05);

    @media screen and (max-width: 480px) {
      transform: none;
    }
  }
`;

export const AccordionContent = styled.div`
  background: transparent;
  color: white;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;

  overflow: hidden;
  transition: all 600ms ease-in-out;

  max-height: ${({show}) => (show ? '150px' : '0')};
  opacity: ${({show}) => (show ? '1' : '0')};

  p {
    font-size: 1.2rem;
    width: 85%;
    padding: 20px 0;

    @media screen and (max-width: 880px) {
      font-size: 1rem;
    }
  }
`;