export const AccordionData = [
    {
        question: "Est ce qu’il est possible de modifier un bloc une fois qu’il a été miné ?",
        answer: "Non, en cas de modification d’un bloc, toutes les modifications des blocs précédents sont falsifiées."
    },
    {
        question: "Quelle est la différence entre un coin et un token ?",
        answer: "Un coin est le jeton natif d’une Blockchain. Le BTC est le coin du Bitcoin, l’ETH est le coin du l’Ethereum. Un token est un jeton non natif d’une blockchain. Certaines blockchains implémentant les smart contracts peuvent accueillir des smart contracts eux même définissant une tokenomics (économie de tokens)."
    },
    {
        question: "Qu’est ce qu’une attaque de 51% sur une blockchain ?",
        answer: "Comme vous l’avez compris, un bloc est validé, si la proof of work ou la proof of stake a été réalisée. Une attaque à 51% proviendrait d’un groupe disposant de plus de 51% des capacités de minage de la chaîne, pouvant ainsi provoquer une double dépense. C'est-à-dire utiliser une première adresse A qui va donner de l’argent a une adresse B. La transaction est ensuite effacée."
    },
    {
        question: "Qui stocke les données de la blockchain ?",
        answer: "Tous les participants à la blockchain stockent une copie des données, ce qu’il fait d’elle l’un des systèmes de stockage le plus sûr au monde.Un bloc de blockchain est approximativement 1Mib."
    },
    {
        question: "Du coup suis je totalement anonyme avec l’achat de d’un jeton Bitcoin ?",
        answer: "Votre adresse publique est effectivement anonyme, mais c’est la conversion en monnaie fiduciaire qui n’est plus anonyme."
    }
]