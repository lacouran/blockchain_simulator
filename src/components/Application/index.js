import React, { useState, useEffect } from "react";
import {
  ApplicationContainer,
  ApplicationContent,
  Tittle,
  Content,
  SubTittle,
} from "./ApplicationElements";
import { SectionH1, SectionH2 } from "../SharedElements";
import BeepleNft from "../../images/assets/beeple-nft.jpg";

const Application = () => {
  return (
    <ApplicationContainer id="application">
      <SectionH1>Application</SectionH1>
      <ApplicationContent>
        <Tittle>Des application pratiques de la blockchain</Tittle>
        <Content>
          <SubTittle>Smart Contract (exemple d'Ethereum)</SubTittle>
          <p>
            Un Smart Contract, ou “contrat intelligent" est une fonctionnalité
            révolutionnaire de la blockchain. Elle permet l'exécution de
            programmes sur celle-ci. Comme dit dans l’introduction, une
            blockchain est un registre de transactions entre plusieurs adresses.
            Un smart contract est un programme qui, lorsque l’on interagit avec
            celui-ci, exécute son code. En clair, lorsqu’une adresse effectue
            une transaction vers l’adresse où réside le smart contrat, celui-ci
            réagit en exécutant son code. Un smart contrat est lui-même une
            adresse comportant un solde et pouvant recevoir et envoyer des
            transactions. Lorsqu’un développeur écrit un smart contrat, il
            l’envoie ensuite à la Machine Virtuelle (EVM, Ethereum Virtual
            Machine), qui va le compiler en bytes code et le stocker dans une
            adresse en l’envoyant par une transaction. Ainsi, le smart contrat
            sera persistant et immuable. C’est là que prend tout l'intérêt d’un
            smart contrat : il est persistant et non modifiable. Cela va donc
            donner lieu à de multiples utilisations que nous allons voir par la
            suite.
          </p>
          <SubTittle>Decentralized autonomous organisation (DAO)</SubTittle>
          <p>
            Le concept de DAO est arrivé sur les Blockchains pourvu de Smart
            Contract. Comme nous l'avons dit précédemment un smart contrat est
            imputable, persistant et bien entendu public : Tout le monde peut
            consulter le code source du bytecode résidant sur la blockchain. Une
            DAO (decentralized autonomous organization, ou organisation autonome
            décentralisée) est une organisation dont les règles de gouvernance
            sont établies par des smarts contrat sur une blockchain. Ainsi, ces
            règles sont publiques, claires et surtout immuables. Les membres de
            cette DAO sont actionnaires de celle-ci et détiennent des jetons de
            gouvernance de celle-ci. En effet, répétons le, un smart contract
            réagit lorsqu’on crée une transaction entrete vers lui. Les jetons
            de gouvernance permettent cela. Un actionnaire possédant beaucoup de
            jetons de gouvernance aura un poids de décision supérieurs à ceux en
            tant que très peu. Un exemple très simple d’application de ces
            jetons est le vote. Toute organisation, qu’elle soit décentralisée
            ou non, procède à des votes pour la gouvernance. Un DAO implémente
            irrémédiablement un smart contrat de vote pour sa gouvernance, ou
            les membres de celle-ci vont envoyer des jetons à celui-ci pour
            voter. La logique du smart contract étant public, la décision ne
            peut être falsifiée. Les DAO peuvent être tout type d’organisation :
            de l'œuvre de charité au projet high tech nécessitant des gros
            moyens.
          </p>
          <SubTittle>Non-fungible token (NFT)</SubTittle>
          <p>
            Les NFT (Non Fongible Token) sont un type particulier de token qui
            résident sur la blockchain. Contrairement à un token, un token non
            fongible est unique. En effet, un token est un jeton d’une économie
            tout entière. Un jeton BTC est une pièce parmi les 21 millions de
            BTC existant. Et 2 BTC sont interchangeables. Il est est de même
            pour la monnaie fiduciaire : 2 pièces de monnaies sont
            interchangeables. Un NFT, lui, va se caractériser par le fait qu’il
            est unique sur toute la blockchain. C’est un type de jeton qui a
            connu un fort succès en 2021 dans le domaine artistique. En effet,
            les NFT sont devenues un mode de vente et d’échange d'œuvres d’art
            numérique. Nous pouvons citer par exemple l’oeuvre “Everydays: the
            First 5 000 Days” par l’artiste Beeple qui s’est vendu pour la somme
            de 69,3 millions de dollars aux enchères sous forme de NFT sur la
            blockchain Ethereum. La blockchain joue donc un rôle de certificat
            d’authenticité et d'appartenance. Les transactions persistantes et
            immuables du NFT permettent à chaque instant d’identifier l'adresse
            blockchain du propriétaire du NFT.
          </p>
          <img src={BeepleNft} id="nft" />
          <p id="legend">Everydays: the First 5 000 Days - Beeple</p>
        </Content>
      </ApplicationContent>
    </ApplicationContainer>
  );
};

export default Application;
