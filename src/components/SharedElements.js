import styled from "styled-components";

export const ButtonJoinWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 4rem;
  max-width: 75%;

  @media screen and (max-width: 768px) {
    padding-top: 2rem;
  }
`;

export const ButtonJoin = styled.a`
    border-radius: 10px;
    background: ${props => props.theme.primary};
    /* white-space: nowrap; */
    padding: 18px 55px;
    color: ${props => props.theme.background};
    font-family: 'Akira Expanded', sans-serif;
    font-size: 1rem;
    font-weight: 900;
    text-align: center;
    outline: none;
    border: none;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    transition: transform 0.5s ease-in-out, background 0.5s ease-in-out;
    text-decoration: none;
    max-width: 100%;

    &:hover {
        transition: transform 0.5s ease-in-out, background 0.5s ease-in-out;
        background: ${props => props.theme.secondary};
        transform: ${({ small }) => (small ? 'none' : 'translateY(-5px)')};
    }

    @media screen and (max-width: 480px) {
      font-size: ${({ small }) => (small ? '0.8rem' : '1.3rem')};
      padding: ${({ small }) => (small ? '10px 22px' : '15px 45px')};
    }
`;

export const SectionH1 = styled.h1`
  font-size: 4.5rem;
  font-family: 'Chakra Petch', sans-serif;
  font-weight: 900;
  color: ${props => props.theme.transparent};
  -webkit-text-stroke: 2px ${props => props.theme.primary};
  margin: 32px 0 64px 0;
  text-align: center;
  max-width: 75%;
  text-transform: uppercase;
  overflow: hidden;

  @media screen and (max-width: 782px) {
    font-size: 3rem
  }

  @media screen and (max-width: 530px) {
    font-size: 2rem;
  }
`;

export const SectionH2 = styled.h2`
  font-size: 1.2rem;
  font-weight: 500;
  color: white;
  margin-bottom: 64px;
  text-align: center;
  max-width: 65%;
  line-height: 1.5;
  overflow: hidden;

  @media screen and (max-width: 768px) {
    max-width: 80%;
  }

  @media screen and (max-width: 480px) {
    font-size: 1rem;
    margin-bottom: 32px;
  }
`;