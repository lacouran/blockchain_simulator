import React from "react";
import { IntroContainer, IntroContent, Tittle, Content } from "./IntroElements";
import { SectionH1, SectionH2 } from "../SharedElements";

const Intro = () => {
  return (
    <IntroContainer id="introduction">
      <SectionH1>Introduction</SectionH1>
      <IntroContent>
        <Tittle>Objectifs</Tittle>
        <Content>
          <p>
            Ce site à pour objectif pédagogique d’initier aux concepts
            fondamentaux de la Blockchains et de l’écosystème qui l'entoure. Le
            grand public n’étant pas initié à ces fondamentaux, il peut voir
            généralement la Blockchain comme un concept assez vague qui se cache
            derrière le “Bitcoin”. Mais cette technologie est bien plus qu’un
            simple code caché derrière le Bitcoin. Au cours de votre lecture,
            vous allez comprendre ce qu’est cette technologie, et pourquoi elle
            est à la base des crypto-monnaies. Nous nous restreindrons à l’usage
            des blockchains dites “publiques”.
          </p>
        </Content>
        <Tittle>Prologue</Tittle>
        <Content>
          <p>
            La monde de la Blockchain, et plus vulgairement de la
            crypto-monnaies, s’est beaucoup démocratisé ces dernières années.
            D’abord restreintes à une seule blockchain : le Bitcoin. Les
            cryptomonnaies sont désormais un acteur non négligeable de
            l’économie et envahissent maintenant le marché avec entre 10 000 et
            20 000 crypto-monnaies actuellement en circulation et listées sur
            les plus grosses plateformes d'échange.
          </p>
        </Content>
        <Tittle>Blockchain , Crypto, quelle différence ?</Tittle>
        <Content>
          <p>
            Ces 2 concepts sont souvent confondus à tort et à travers dans la
            littérature, et cela pour l’unique raison qu’une crypto-monnaie
            repose toujours sur une blockchain. L’inverse n’est en revanche pas
            vrai : une blockchain est une technologie générique. Alors pourquoi
            la blockchain est systématiquement utilisée dans le cadre d’une
            crypto-monnaie ? La réponse réside dans un seul mot : la
            cryptographie. En effet, cette discipline est aujourd’hui la manière
            plus plus fiable pour assurer une sécurité dans un environnement
            décentralisé. La cryptographie est au cœur du fonctionnement de la
            blockchain.
          </p>
        </Content>
      </IntroContent>
    </IntroContainer>
  );
};

export default Intro;
