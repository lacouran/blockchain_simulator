import styled from 'styled-components';

export const IntroContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  /* min-height: 100vh; */
  width: 100%;
  background: linear-gradient(0deg, transparent 50%, black 100%);
  padding-bottom: 50px;
`;

export const IntroContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  width: 80%;
  color: white;
`;

export const Tittle = styled.h2`
  font-family: 'Chakra Petch', sans-serif;
  width: 100%;
  font-size: 2rem;
`;

export const Content = styled.div`
  p {
    font-size: 1.1rem;
    text-align: justify;
  }
`;