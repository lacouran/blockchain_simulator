export const ExpansionColors = {
  primary : "#FFFFFF",
  secondary : "#2EFAFA",
  black : "#000000",
  background : "#0A101E",
  background2 : "#0B1328",
  transparent : "transparent"
}

/* To rgb :
  primary #FFFFFF : rgb(255, 255, 255)
  secondary #2EFAFA : rgb(46, 250, 250)
  background #0B1329 : rgb(11, 19, 41)
*/