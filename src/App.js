import './App.css';
import React, { useState, useEffect } from 'react';
import Home from './pages/Home';
import SimulatorPage from './pages/Simulator';
import {
  BrowserRouter,
  Routes,
  Route,
  Link,
} from "react-router-dom";

function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        {/* <Route path="simulator/*" element={<SimulatorPage />} /> */}
      </Routes>
    </BrowserRouter>
  )
}

export default App;
