import hashlib
import json
import random
from time import time
from uuid import uuid4
from urllib.parse import urlparse
from operator import itemgetter

from keyGenerator import generateKey
from flask import Flask, jsonify, request
from flask_cors import CORS

MACHINE_address = "bc1qar0srrr7xfkvy5l643lydnw9re59gtzzwf5mdq"


class address:
    def __init__(self):
        self.address = str(uuid4()).replace('-', '')
        self.balance = 10

    def serialize(self):
        return {
            'address': self.address,
            'balance': self.balance
        }


class mineur:
    def __init__(self, address, balance):
        self.address = address
        self.balance = balance

    def serialize(self):
        return {
            'address': self.address,
            'balance': self.balance
        }


addressList = [address() for i in range(0, 100)]  #
mineurList = [mineur(str(uuid4()).replace('-', ''), 3) for i in range(0, 5)]


class BlockChain(object):
    """ Main BlockChain class """

    def __init__(self):
        self.chain = []
        self.current_transactions = []
        self.nodes = set()
        # create the genesis block
        self.new_block(previous_hash=1, proof=100)

    def kill(self):
        del self

    @staticmethod
    def hash(block):
        # hashes a block
        # also make sure that the transactions are ordered otherwise we will have insonsistent hashes!
        block_string = json.dumps(block, sort_keys=True).encode()
        return hashlib.sha256(block_string).hexdigest()

    def new_block(self, proof, previous_hash=None):
        # creates a new block in the blockchain
        self.current_transactions = sorted(self.current_transactions, key=itemgetter('fees'), reverse=True)
        for transaction in self.current_transactions[0:4]:
            transaction["status"] = "success"
        block = {
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'transactions': self.current_transactions[0:4],
            'proof': proof,
            'previous_hash': previous_hash or self.hash(self.chain[-1]),
        }

        # reset the current list of transactions
        self.current_transactions = self.current_transactions[4:]
        self.chain.append(block)
        return block

    @property
    def last_block(self):
        # returns last block in the chain
        return self.chain[-1]

    def new_transaction(self, sender, recipient, amount, fees):
        # adds a new transaction into the list of transactions
        # these transactions go into the next mined block
        self.current_transactions.append({
            "sender": sender,
            "recipient": recipient,
            "data": amount,
            "status": "pending",
            "fees": fees
        })
        return int(self.last_block['index']) + 1

    def proof_of_work(self, last_proof):
        # simple proof of work algorithm
        # find a number p' such as hash(pp') containing leading 4 zeros where p is the previous p'
        # p is the previous proof and p' is the new proof
        proof = 0
        while self.validate_proof(last_proof, proof) is False:
            proof += 1
        return proof

    @staticmethod
    def validate_proof(last_proof, proof):
        # validates the proof: does hash(last_proof, proof) contain 4 leading zeroes?
        guess = f'{last_proof}{proof}'.encode()
        guess_hash = hashlib.sha256(guess).hexdigest()
        return guess_hash[:4] == "0000"

    def register_node(self, address):
        # add a new node to the list of nodes
        parsed_url = urlparse(address)
        self.nodes.add(parsed_url.netloc)

    def full_chain(self):
        # xxx returns the full chain and a number of blocks
        pass


# initiate the node
from pos import posFile

app = Flask(__name__)
CORS(app)
# generate a globally unique address for this node

app.register_blueprint(posFile)

node_identifier = str(uuid4()).replace('-', '')
# initiate the Blockchain
blockchain = BlockChain()


@app.route('/pow/reset', methods=['GET'])
def reset():
    try:
        global blockchain
        del blockchain
        blockchain = BlockChain()
        response = {
            'chain': blockchain.chain,
            'length': len(blockchain.chain),
        }
        return jsonify(response), 200


    except:
        return json.dumps({'success': False}), 400, {'ContentType': 'application/json'}


@app.route('/pow/mine', methods=['GET'])
def mine():
    if len(blockchain.current_transactions) >= 3:
        # first we need to run the proof of work algorithm to calculate the new proof.
        last_block = blockchain.last_block
        last_proof = last_block['proof']
        proof = blockchain.proof_of_work(last_proof)
        global addressList
        winner = random.choice(mineurList).address

        for elem in blockchain.current_transactions[0:4]:

            for _, item in enumerate(addressList):
                if item.address == elem['recipient']:
                    item.balance = item.balance + int(elem['data'])
            for _,item in enumerate(mineurList):
                if item.address==winner:
                    item.balance+=int(elem['fees'])




        # we must recieve reward for finding the proof in form of receiving 1 Coin
        fees = 0
        blockchain.new_transaction(
            sender=MACHINE_address,
            recipient=winner,
            amount=1,
            fees=0
        )

        # forge the new block by adding it to the chain
        previous_hash = blockchain.hash(last_block)

        block = blockchain.new_block(proof, previous_hash)

        response = {
            'message': "Forged new block",
            'index': block['index'],
            'transactions': block['transactions'],
            'proof': block['proof'],
            'previous_hash': block['previous_hash'],
        }
        return jsonify(response, 200)
    else:
        return jsonify({'success': False, "error": "there is no transaction right now"}), 400, {
            'ContentType': 'application/json'}


@app.route('/pow/transaction/new', methods=['POST'])
def new_transaction():
    amount = request.values.get('amount')
    recipient = request.values.get('recipient')
    sender = request.values.get('sender')
    print("Fees are " + str(request.values.get('fees')))
    fees = 1 if int(request.values.get('fees')) is None else int(request.values.get('fees'))
    if not sender and not recipient and not amount:
        return 'Missing values.', 400

    for _, item in enumerate(addressList):
        if item.address == sender:
            if item.balance - fees >= 0:
                item.balance = item.balance - fees
            else:
                return 'Not Enough Money', 400
            if item.balance - int(amount) >= 0:

                item.balance = item.balance - int(amount)
                print(item.balance)
            else:
                return 'Not Enough Money', 400

    # create a new transaction

    index = blockchain.new_transaction(
        sender=sender,
        recipient=recipient,
        amount=amount,
        fees=fees
    )

    response = {
        'message': f'Transaction will be added to the Block {index}',
    }
    return jsonify(response, 200)


@app.route("/pow/transactionActuel", methods=['GET'])
def transactionAll():
    listTransaction = blockchain.current_transactions
    print(listTransaction)
    listTransaction = sorted(listTransaction, key=itemgetter('fees'), reverse=True)
    # print(listTransaction)
    return jsonify(listTransaction)


@app.route('/pow/chain', methods=['GET'])
def full_chain():
    response = {
        'chain': blockchain.chain,
        'length': len(blockchain.chain),
    }
    return jsonify(response), 200


@app.route('/pow/dictAddress', methods=['GET'])
def dictAddress():
    addressListJson = []
    for elem in addressList:
        addressListJson.append(elem.serialize())

    return jsonify(addressListJson), 200


@app.route('/pow/listaddress', methods=['GET'])
def listaddress():
    addressListJson = []
    for elem in addressList:
        addressListJson.append(elem.address)

    return jsonify(addressListJson), 200


@app.route('/pow/listMineur', methods=['GET'])
def listMineur():
    mineurListJson = []
    for elem in mineurList:
        mineurListJson.append(elem.serialize())

    return jsonify(mineurListJson), 200


@app.route('/pow/addMineur', methods=['POST'])
def addMineur():
    amount = request.values.get('amount')
    address = request.values.get('address')
    newMineur = mineur(address, int(amount))
    mineurList.append(newMineur)
    return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}


@app.route('/pow/deleteMineur', methods=['POST'])
def deleteMineur():
    address = request.values.get('address')
    for index, elem in enumerate(mineurList):
        if elem.address == address:
            mineurList.pop(index)
            return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
    return json.dumps({'error': True}), 400, {'ContentType': 'application/json'}


@app.route('/pow/credidateAddress', methods=['POST'])
def credidate():
    amount = request.values.get('amount')
    print(amount)
    address = request.values.get('address')
    # print(address)
    for _, item in enumerate(addressList):
        if item.address == address:
            item.balance += int(amount)
            # print(item.serialize())
            return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
    else:
        return json.dumps({'error': True}), 400, {'ContentType': 'application/json'}


@app.route('/generateKey', methods=['GET'])
def generate_key():
    dictKey = generateKey()
    print(dictKey)
    return jsonify(dictKey)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
